let arr = ['Kiev', 'Lviv', "Odessa","Chernihiv"];

let lists = document.getElementById("lists");

function createList(arr) {
    let ul = document.createElement('ul');
    let newArr = arr.map(function (elem) {
        let li = document.createElement("li");
        let newList;
        if (Array.isArray(elem)) {
            newList = createList(elem);
        } else if (typeof elem === 'object' && !Array.isArray(elem)) {
            let dopList = Object.keys(elem);
            newList = createList(dopList)
        } else newList = document.createTextNode(elem);

        li.appendChild(newList);
        ul.appendChild(li);
    });
    lists.appendChild(ul);
    return ul
}

createList(arr);